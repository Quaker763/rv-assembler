/**
 *  Copyright (c) 2018-2019 Jesse Buhagiar
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 *
 * numerical.hpp
 *
 *  Created on: 21 Nov. 2018
 *      Author: Jesse Buhagiar
 */

#ifndef INCLUDE_NUMERICAL_HPP_
#define INCLUDE_NUMERICAL_HPP_

#include <string>

/*
 * Is the given number a hex digit or not?
 */
bool isHexNumber(const std::string& num);

/*
 * Is the given number a binary digit or not?
 */
bool isBinaryNumber(const std::string& num);

/**
 * Check if a string contains a specific character
 */
bool stringContains(const std::string& str, char c);

/**
 * Better wrapper for stoul
 *
 * This can be broken by ASCII!!! So be careful!
 */
template <typename T>
T Str2Num(const std::string& num)
{
    T ret;

    if(isHexNumber(num))
        ret = std::stoll(num, NULL, 16);
    else if(isBinaryNumber(num))
        ret = std::stoll(num, NULL, 2);
    else
        ret = std::stoll(num, NULL, 10);

    return ret;
}


#endif /* INCLUDE_NUMERICAL_HPP_ */
