/**
 *  Copyright (c) 2018-2019 Jesse Buhagiar
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 *
 * file.hpp: Assembly source file loader.
 *
 *  Created on: 25 Oct. 2018
 *      Author: Jesse Buhagiar
 */

#ifndef INCLUDE_FILE_HPP_
#define INCLUDE_FILE_HPP_

#include <string>
#include <vector>
#include <fstream>

#include "common.hpp"

/**
 * Logical source file
 */
class CFile
{
public:

    /**
     *  Open status enum (for this file)
     */
    enum class OpenStatus
    {
        SUCCESS,
        IO_ERROR,
        READ_FAIL,
    };

    // Constructors
public:
    /**
     *
     */
    CFile(const std::string& path = "");

    /**
     * Copy constructor
     */
    CFile(const CFile& file);

    // Member functions
public:
    /**
     *  Read the file into the @ref lines vector
     */
    OpenStatus Read(const std::string fname);
    OpenStatus Read();


    /**
     * Get a single line of code from the file
     */
    const __forceinline std::string& GetLine(size_t line) const{return lines.at(line);}

    /**
     * Get the number of lines in this file
     */
    size_t __forceinline GetNumberOfLines() const{return nlines;}

    /**
     *
     */
    void __forceinline SetFilePath(const std::string& path){fname = path;}

    /**
     *
     */
    const std::string GetName() const;

    /**
     * Tokenize a line into a string array
     */
    std::vector<std::string> SplitLine(const size_t line) const;

    /**
     * Remove a line from the file
     */
    void RemoveLine(const size_t line);

private:
    std::ifstream               fhandle;
    std::string                 fname;  /**< Name of this source file */
    std::vector<std::string>    lines;  /**< Each source line */

    size_t                      nlines;
};








#endif /* INCLUDE_FILE_HPP_ */
